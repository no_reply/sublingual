injecting estradiol
===================

i've been avoiding having anything like a web presence since i quit twitter
in 2020.

this is me going from zero to overengineered in a single shot.

[flux][flux]
----

```sh
GITLAB_TOKEN=[REDACTED] KUBECONFIG=~/.kube/estradiol-cloud-kubeconfig.yaml flux bootstrap gitlab \
  --owner=no_reply \
  --repository=sublingual \
  --branch=trunk \
  --path=estradiol.cloud \
  --personal
```


[flux]: https://fluxcd.io/flux
