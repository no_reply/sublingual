if status is-interactive
  set -U EDITOR emacs
  set -U KUBECONFIG /home/tamsin/.kube/estradiol-cloud-kubeconfig.yaml
end

alias assume="source /usr/local/bin/assume.fish"
