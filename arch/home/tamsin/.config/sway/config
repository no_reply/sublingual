# Default config for sway
set $mod Mod4
set $left h
set $down j
set $up k
set $right l
set $term foot
set $menu fuzzel -b 000000ff -f SourceCodePro:size=10 -i Arc -w 40 -l 25
exec mako
output * bg /usr/share/backgrounds/sway/rays.jpg fill
output eDP-1 resolution 3840x2400 position 0 0
output DP-1 resolution 1920x1080 position  0 1
input "1:1:AT_Translated_Set_2_keyboard" {
    xkb_layout "us"
    xkb_variant "dvorak"
    xkb_options "ctrl:nocaps"
}

### Key bindings
#
# Basics:
#
    bindsym $mod+Shift+Return exec $term
    bindsym $mod+Shift+q kill
    bindsym $mod+d exec $menu
    floating_modifier $mod normal
    bindsym $mod+Shift+c reload
    bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'
#
# Moving around:
#
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# mako notifactions
#
    bindsym $mod+c exec makoctl dismiss -g
    bindsym $mod+t exec makoctl restore
#
# Workspaces:
#
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    bindsym $mod+7 workspace number 7
    bindsym $mod+8 workspace number 8
    bindsym $mod+9 workspace number 9
    bindsym $mod+0 workspace number 10
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    bindsym $mod+Shift+0 move container to workspace number 10
    workspace 1 output eDP-1
    workspace 2 output eDP-1
    workspace 3 output eDP-1
    workspace 4 output eDP-1
    workspace 5 output eDP-1
    workspace 6 output eDP-1
    workspace 7 output DP-1
    workspace 8 output DP-1
    workspace 9 output DP-1
    workspace 10 output DP-1
#
# Layout stuff:
#
    bindsym $mod+b splith
    bindsym $mod+v splitv
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split
    bindsym $mod+f fullscreen
    bindsym $mod+Shift+space floating toggle
    bindsym $mod+space focus mode_toggle
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    bindsym $mod+Shift+minus scratchpad move
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

include /etc/sway/config.d/*
